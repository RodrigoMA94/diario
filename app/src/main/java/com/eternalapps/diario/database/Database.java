package com.eternalapps.diario.database;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.eternalapps.diario.model.Days;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import static com.eternalapps.diario.constants.Constants.DB_NAME;
import static com.eternalapps.diario.constants.Constants.DB_PATH;
import static com.eternalapps.diario.constants.Constants.DB_VERSION;

public class Database extends SQLiteOpenHelper {
    private Context context;

    public Database(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.context = context;
    }

    public void open() throws SQLException {
        try {
            createDB();
        } catch (IOException e) {
            throw new Error("No se ha podido crear la BD");
        }
    }

    private void createDB() throws IOException {
        boolean dbExist = checkDB();

        if (dbExist) {
            this.getWritableDatabase();
        } else {
            this.getReadableDatabase();

            try {
                copyDB();
            } catch (IOException e) {
                throw new Error("Error copying DB");
            }
        }
    }

    private boolean checkDB() {
        SQLiteDatabase sqLiteDatabase = null;

        try {
            String myPath = DB_PATH + DB_NAME;
            sqLiteDatabase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READONLY);
        } catch (SQLException e) {
            //db doesnt exists yet
        }

        if (sqLiteDatabase != null) {
            sqLiteDatabase.close();
        }

        return sqLiteDatabase != null;
    }

    private void copyDB() throws IOException {
        InputStream inputStream = context.getAssets().open(DB_NAME);
        String outFileName = DB_PATH + DB_NAME;
        OutputStream outputStream = new FileOutputStream(outFileName);

        byte[] buffer = new byte[1024];
        int length;
        while ((length = inputStream.read(buffer)) > 0) {
            outputStream.write(buffer, 0, length);
        }
        outputStream.flush();
        outputStream.close();
        inputStream.close();
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    // Método para obtener listado de días
    public ArrayList<Days> getDayList() {
        ArrayList<Days> list = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        String query = "SELECT id, day, title, description, img FROM days";
        String[] params = {};
        Cursor cursor = sqLiteDatabase.rawQuery(query, params);

        if (cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(cursor.getColumnIndex("id"));
                String day = cursor.getString(cursor.getColumnIndex("day"));
                String title = cursor.getString(cursor.getColumnIndex("title"));
                String description = cursor.getString(cursor.getColumnIndex("description"));
                String img = cursor.getString(cursor.getColumnIndex("img"));

                Days days = new Days(id, day, title, description, img);
                list.add(days);
            } while (cursor.moveToNext());
        }
        cursor.close();
        sqLiteDatabase.close();

        return list;
    }

    public Days getDescriptionData(int id) {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();

        String table = "days";
        String columns[] = {"title", "description", "img"};
        String where = "id = ?";
        String[] args = {String.valueOf(id)};

        Cursor cursor = sqLiteDatabase.query(table, columns, where, args, null, null, null);
        String title = "", desc = "", img = "";

        if (cursor.moveToFirst()) {
            do {
                title = cursor.getString(cursor.getColumnIndex("title"));
                desc = cursor.getString(cursor.getColumnIndex("description"));
                img = cursor.getString(cursor.getColumnIndex("img"));

            } while (cursor.moveToNext());
        }

        Days days = new Days();
        days.setDescTitle(title);
        days.setDescBody(desc);
        days.setDescImg(img);

        cursor.close();
        sqLiteDatabase.close();

        return days;
    }
}

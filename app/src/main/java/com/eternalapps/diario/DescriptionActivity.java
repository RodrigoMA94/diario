package com.eternalapps.diario;

import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.eternalapps.diario.database.Database;
import com.eternalapps.diario.decor.CustomTextView;
import com.eternalapps.diario.methods.Methods;
import com.eternalapps.diario.model.Days;

public class DescriptionActivity extends AppCompatActivity implements View.OnClickListener {
    private Methods methods = new Methods(this);
    private int id;
    private String title, desc;
    private Drawable img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description);

        CustomTextView tv_title, tv_desc;
        ImageView iv_bg;
        Button bt_back;

        tv_title = findViewById(R.id.tv_title);
        tv_desc = findViewById(R.id.tv_desc);
        iv_bg = findViewById(R.id.iv_bg);
        bt_back = findViewById(R.id.bt_back);

        methods.windowStyle(this);

        getId();
        getData();

        tv_title.setText(title);
        tv_desc.setText(desc);
        iv_bg.setImageDrawable(img);

        bt_back.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        methods.windowStyle(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_back:
                finish();
                break;
        }
    }

    private void getId() {
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            id = bundle.getInt("id");
        }
    }

    private void getData() {
        Database database = new Database(this);
        database.open();

        Days days = database.getDescriptionData(id);
        title = days.getDescTitle();
        desc = days.getDescBody();
        img = methods.getImgFromAssets(this, days.getDescImg());
    }
}

package com.eternalapps.diario.methods;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.WindowManager;

import com.eternalapps.diario.R;
import com.readystatesoftware.systembartint.SystemBarTintManager;

import java.io.IOException;
import java.io.InputStream;

public class Methods {
    private Context context;

    public Methods(Context context) {
        this.context = context;
    }

    // Decor window for MainCoreActivity
    public void windowStyle(Activity activity) {
        // Set transparent Status Bar for Api >= 21
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            ((Activity) context).getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            SystemBarTintManager tintManager = new SystemBarTintManager(activity);
            tintManager.setStatusBarTintEnabled(true);
            tintManager.setTintColor(ContextCompat.getColor(context, R.color.colorBlack));
        } else if (Build.VERSION.SDK_INT >= 21) {
            ((Activity) context).getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        // Hide Navigation Bar
        View decorView = ((Activity) context).getWindow().getDecorView();

        int uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;

        decorView.setSystemUiVisibility(uiOptions);
    }

    // Función para cargar una imagen desde assets
    public Drawable getImgFromAssets(Context context, String name) {
        try {
            if (name != null) {
                InputStream ims = context.getAssets().open(name);
                return Drawable.createFromStream(ims, null);
            }

        } catch (IOException ex) {
            // catch
        }

        return null;
    }
}

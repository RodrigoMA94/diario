package com.eternalapps.diario;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.eternalapps.diario.database.Database;
import com.eternalapps.diario.methods.Methods;
import com.eternalapps.diario.model.Days;
import com.eternalapps.diario.adapter.DaysAdapter;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private Methods methods = new Methods(this);
    private RecyclerView main_rv;
    private Database db;
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        main_rv = findViewById(R.id.main_rv);

        methods.windowStyle(this);

        db = new Database(this);
        db.open();
        ArrayList<Days> dayList = db.getDayList();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        main_rv.setLayoutManager(layoutManager);

        main_rv.setHasFixedSize(true);
        DaysAdapter adapter = new DaysAdapter(dayList);
        main_rv.setAdapter(adapter);

        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int i = main_rv.getChildAdapterPosition(view);

                Days days = db.getDayList().get(i);
                int id = days.getDayId();

                Intent intent = new Intent(MainActivity.this, DescriptionActivity.class);
                intent.putExtra("id", id);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        methods.windowStyle(this);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Pulsa otra vez para salir", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 3000);
    }
}

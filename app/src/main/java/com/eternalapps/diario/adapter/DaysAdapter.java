package com.eternalapps.diario.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eternalapps.diario.R;
import com.eternalapps.diario.decor.CustomTextView;
import com.eternalapps.diario.model.Days;

import java.util.List;

public class DaysAdapter extends RecyclerView.Adapter<DaysAdapter.DaysViewHolder> implements View.OnClickListener {
    private List<Days> items;
    private View.OnClickListener listener;

    static class DaysViewHolder extends RecyclerView.ViewHolder {
        CustomTextView tv_day;

        private DaysViewHolder(View itemView) {
            super(itemView);
            tv_day = itemView.findViewById(R.id.tv_row);
        }
    }

    public DaysAdapter(List<Days> items) {
        this.items = items;
    }

    @Override
    public DaysViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_main, parent, false);
        v.setOnClickListener(this);
        return new DaysViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final DaysViewHolder holder, final int position) {
        holder.tv_day.setText(items.get(position).getRvDay());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if (listener != null)
            listener.onClick(view);
    }
}

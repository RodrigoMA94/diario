package com.eternalapps.diario.constants;

import android.annotation.SuppressLint;

public class Constants {
    public static final String DB_NAME = "DiarioDB.sqlite";
    public static final int DB_VERSION = 1;
    @SuppressLint("SdCardPath")
    public static final String DB_PATH = "/data/data/com.eternalapps.diario/databases/";
}

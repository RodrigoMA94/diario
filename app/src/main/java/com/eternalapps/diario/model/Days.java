package com.eternalapps.diario.model;

public class Days {
    private int dayId;
    private String rvDay, descTitle, descBody, descImg;

    public Days() {
    }

    public Days(int dayId, String rvDay, String descTitle, String descBody, String descImg) {
        this.dayId = dayId;
        this.rvDay = rvDay;
        this.descTitle = descTitle;
        this.descBody = descBody;
        this.descImg = descImg;
    }

    public int getDayId() {
        return dayId;
    }

    public String getRvDay() {
        return rvDay;
    }

    public String getDescTitle() {
        return descTitle;
    }

    public void setDescTitle(String descTitle) {
        this.descTitle = descTitle;
    }

    public String getDescBody() {
        return descBody;
    }

    public void setDescBody(String descBody) {
        this.descBody = descBody;
    }

    public String getDescImg() {
        return descImg;
    }

    public void setDescImg(String descImg) {
        this.descImg = descImg;
    }
}
